package com.example.bookstore.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.bookstore.model.Books;
import com.example.bookstore.repository.BooksRepository;

@Controller
@RequestMapping("/master/")
public class BooksController {
	@Autowired
	private BooksRepository booksRepository;

	@GetMapping("booklist")
	public ResponseEntity<List<Books>> getAllList() {
		try {
			List<Books> booklist = this.booksRepository.findByIsActive(true);
			return new ResponseEntity<>(booklist, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("booklist/category/{category_id}")
	public ResponseEntity<List<Books>> getListByCategory(@PathVariable("category_id") Long category_id) {
		try {
			List<Books> booklist = this.booksRepository.findByCategory(category_id);
			return new ResponseEntity<>(booklist, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("booklist/add")
	public ResponseEntity<Object> addBook(@RequestBody Books books) {
		books.setCreateBy("admin");
		books.setCreateDate(new Date());
		books.setIsActive(true);

		Books newBook = this.booksRepository.save(books);

		if (newBook.equals(books)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Error", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("booklist/edit/{id}")
	public ResponseEntity<Object> editBook(@PathVariable("id") Long id, @RequestBody Books books) {
		Optional<Books> bookData = this.booksRepository.findById(id);

		if (bookData.isPresent()) {
			books.setId(id);
			books.setModifyBy("admin");
			books.setModifyDate(new Date());
			books.setCreateBy(bookData.get().getCreateBy());
			books.setCreateDate(bookData.get().getCreateDate());
			this.booksRepository.save(books);
			return new ResponseEntity<Object>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("booklist/delete/{id}")
	public ResponseEntity<Object> deleteBook(@PathVariable("id") Long id) {
		Optional<Books> bookData = this.booksRepository.findById(id);

		if (bookData.isPresent()) {
			Books books = new Books();
			books.setId(id);
			books.setIsActive(false);
			books.setModifyBy("admin");
			books.setModifyDate(new Date());
			books.setCreateBy(bookData.get().getCreateBy());
			books.setCreateDate(bookData.get().getCreateDate());
			books.setBookName(bookData.get().getBookName());
			books.setAuthor(bookData.get().getAuthor());
			books.setCategoryId(bookData.get().getCategoryId());
			books.setLanguageId(bookData.get().getLanguageId());
			books.setPrice(bookData.get().getPrice());
			books.setSummary(bookData.get().getSummary());
			this.booksRepository.save(books);
			return new ResponseEntity<>("Deleted", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("bookstotal")
	@ResponseBody
	public long getBooksTotal() {
		List<Books> booklist = this.booksRepository.findByIsActive(true);
		long booksTotal = booklist.stream().count();
		
		return booksTotal;

	}
	
	@GetMapping("maxprice")
	@ResponseBody
	public long getMaxPrice() {
		List<Books> booklist = this.booksRepository.findByIsActive(true);
		long maxPrice = booklist.stream().mapToLong(Books::getPrice).max().orElse(0);
		
		return maxPrice;

	}
	
	@GetMapping("minprice")
	@ResponseBody
	public long getMinPrice() {
		List<Books> booklist = this.booksRepository.findByIsActive(true);
		long minPrice = booklist.stream().mapToLong(Books::getPrice).min().orElse(0);
		
		return minPrice;

	}

}
