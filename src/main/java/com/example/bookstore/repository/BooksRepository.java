package com.example.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.bookstore.model.Books;

public interface BooksRepository extends JpaRepository<Books, Long> {
	List<Books> findByIsActive(Boolean isActive);
	
	@Query(value="SELECT * FROM books WHERE category_id= ?1 AND is_active=true", nativeQuery=true)
	List<Books> findByCategory(Long categoryId);

}

